@extends('main')

@section('content')
    <div class="starter-template">
        <h1>Все книги</h1>
        <div id="books" v-cloak>
            <div class="row row-flex">
                <div v-for="book in books" class="col-md-4">
                    <div class="panel panel-default main-books">
                        <div class="panel-heading">
                            <h3 class="panel-title">@{{ book.name }}</h3>
                        </div>
                        <div class="panel-body">
                            <strong>@{{ book.title }}</strong>
                            <p>
                                <img v-bind:src="book.image">
                            </p>
                            <strong>Автор:</strong>
                            <ul>
                                <li v-for="author in book.authors">@{{ author.name }}</li>
                            </ul>
                            <p>@{{ book.description }}</p>
                            <p>
                                <a class="btn btn-success show-book" v-bind:href="'/api/book/' + book.id">
                                    Смотреть книгу <span class="glyphicon glyphicon-arrow-right"
                                                         aria-hidden="true"></span>
                                </a>
                            </p>
                            <span class="glyphicon glyphicon-tags" aria-hidden="true"></span>
                            <span v-for="tag in book.tags" class="label label-primary tags">
                                <a v-bind:href="'/api/books-tag/' + tag.tag">@{{ tag.tag }}</a>
                            </span>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
@endsection
