<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('api')->get('/books', 'Api\BookController@index')->name('get-books');
Route::middleware('api')->get('/book/{id}', 'Api\BookController@show')->name('show-book');
Route::middleware('api')->get('/books-tag/{tag}', 'Api\BookController@tag')->name('get-book-tag');
Route::middleware('api')->post('/add-to-cart/{id}', 'Api\BookController@addCart')->name('book-to-cart');
Route::middleware('api')->get('/show-cart', 'Api\BookController@showCart')->name('show-cart');
Route::middleware('api')->post('/subscribe/{id}', 'Api\BookController@subscribe')->name('subscribe');
Route::middleware('api')->post('/cart-clear', 'Api\BookController@clearCart')->name('clear-cart');
