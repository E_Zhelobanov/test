init_dev: composer db_create env_copy migrations seeds key_generate clean

composer:
	composer install

db_create:
	mysql -u homestead -psecret -e "CREATE DATABASE IF NOT EXISTS test CHARACTER SET utf8 COLLATE utf8_general_ci; GRANT ALL PRIVILEGES ON test.* TO test@localhost IDENTIFIED BY 'test';";
	exit;

env_copy:
	cp .env.example .env
	
migrations:
	php artisan migrate:refresh

seeds:
	php artisan db:seed

key_generate:
	php artisan key:generate

clean:
	php artisan clear-compiled
	php artisan cache:clear
	php artisan config:clear
	php artisan route:clear
	php artisan view:clear
