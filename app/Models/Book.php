<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    const STATUS_ENABLE = 1;
    const STATUS_DISABLE = 0;

    /**
     * Get authors of book
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function authors()
    {
        return $this->belongsToMany('App\Models\Author', 'author_books', 'book_id', 'author_id');
    }

    /**
     * Get tags of book
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tags()
    {
        return $this->belongsToMany('App\Models\Tag', 'book_tags', 'book_id', 'tag_id');
    }

    /**
     * Subscribe to book
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function subscribe()
    {
        return $this->hasOne('App\Models\Subscribe');
    }
    
    /**
     * Book to bucket
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function basket()
    {
        return $this->hasOne('App\Models\Basket');
    }
}
