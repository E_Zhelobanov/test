# Test project (Laravel 5.4)

##Installation manual

* clone this repository
* run `make init_dev`

##OR

* clone this repository
* run `composer install`
* copy file `.env.example` as `.env`
* create your database and user
* add in `.env` file your database settings
* run `php artisan migrate`
* run `php artisan db:seed`
* run `php artisan key:generate`
