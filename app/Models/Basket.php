<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Basket extends Model
{
    protected $table = 'basket';
    
    protected $fillable = [
        'number',
    ];
    
    /**
     * Get books in basket
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function books()
    {
        return $this->belongsTo('App\Models\Book', 'book_id');
    }
    
}
