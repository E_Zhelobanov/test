//Vue model list of book
var books = new Vue({
    el: '#books',
    data: {
        books: []
    }
});

//Vue model cart
var cart = new Vue({
    el: '#cart',
    data: {
        bks: []
    },
    methods: {
        rmCart: function () {
            $.ajax({
                type: 'POST',
                url: '/api/cart-clear',
                success: function () {
                    cart.bks = '';
                }
            })
        }
    }
});

//Get list of book
$.ajax({
    type: 'GET',
    url: '/api/books',
    success: function (data) {
        books.books = data;
    }
});

//Get basket
$.ajax({
    type: 'GET',
    url: '/api/show-cart',
    success: function (data) {
        cart.bks = data;
    }
});

//Subscribe
$('#sub').on('click', function () {
    var book_id = $(this).data('book_id');
    $.ajax({
        type: 'POST',
        url: '/api/subscribe/' + book_id,
        success: function () {
            $('#sub').toggleClass("btn-warning btn-primary").hasClass('btn-warning') ? $('#sub').text('Отписаться') : $('#sub').text('Подписаться');
        }
    });
});

//Add book to card
$('#crt').on('click', function () {
    var book_id = $(this).data('book_id');
    $.ajax({
        type: 'POST',
        url: '/api/add-to-cart/' + book_id,
        success: function (data) {
            cart.bks = data;
        }
    });
});
