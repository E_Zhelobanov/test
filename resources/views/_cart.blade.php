<div class="row">
    <div class="col-md-4 col-md-offset-8">
        <div class="panel panel-default">
            <div class="panel-heading">Корзина:</div>
            <div class="panel-body">

                <div id="cart" class="cart" v-cloak>
                    <div v-if="bks == '' ">
                        Корзина пуста
                    </div>

                    <div v-else>
                        <ul>
                            <li v-for="bk in bks"><a v-bind:href="'/api/book/' + bk.id">@{{ bk.name }} (@{{ bk.basket.number }})</a></li>
                        </ul>

                        <button id="remove-cart" type="submit" class="btn btn-danger" v-on:click="rmCart">Очистить корзину</button>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
