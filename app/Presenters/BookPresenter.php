<?php

namespace App\Presenters;

use App\Models\Book;
use App\Models\Tag;
use App\Models\Basket;


class BookPresenter
{
    /**
     * Get all books with authors and tags
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getBooks()
    {
        return Book::with('authors', 'tags')->get();
    }

    /**
     * Get book by ID
     *
     * @param int $id
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
     */
    public function getBookById($id)
    {
        return Book::with('authors', 'tags')->findOrFail($id);
    }

    /**
     * Get books by tag
     *
     * @param string $tag
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getBooksByTag($tag)
    {
        return Tag::with('books')->where('tag', $tag)->first();
    }
    
    /**
     * Subscribe
     *
     * @param int $id
     */
    public function subscribeBook($id)
    {
        $book = Book::find($id);
        isset($book->subscribe) ? $book->subscribe()->delete() : $book->subscribe()->create(['status' => 1]);
    }
    
    /**
     * Get book
     *
     * @param int $id
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function bookAddToCart($id)
    {
        $book = Book::find($id);
        
        if (!is_null($book->basket)) {
            $book->basket->increment('number');
        } else {
            $book->basket()->create(['number' => 1]);
        }
        
        return Book::with('basket')->has('basket')->get();
    }
    
    /**
     * Get my basket
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getMyCart()
    {
        return Book::with('basket')->has('basket')->get();
    }
    
    /**
     * Remove my basket
     */
    public function clearMyCart()
    {
        Basket::truncate();
    }
}
