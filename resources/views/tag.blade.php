@extends('main')

@section('content')
    <div class="starter-template">
        <h1>Фильтр по тегу #{{ $tag->tag }}</h1>

        <div id="books">
            <div class="row row-flex">
                @foreach($tag->books as $book)
                    <div class="col-md-4">
                        <div class="panel panel-default main-books">
                            <div class="panel-heading">
                                <h3 class="panel-title">{{ $book->name }}</h3>
                            </div>
                            <div class="panel-body">
                                <strong>{{ $book->title }}</strong>
                                <p>
                                    <img src="{{ $book->image }}">
                                </p>
                                <strong>Автор:</strong>
                                <ul>
                                    @foreach($book->authors as $author)
                                        <li>{{ $author->name }}</li>
                                    @endforeach
                                </ul>
                                <p>{{ $book->description }}</p>
                                <p>
                                    <a class="btn btn-success show-book" href="/api/book/{{ $book->id }}">
                                        Смотреть книгу <span class="glyphicon glyphicon-arrow-right"
                                                             aria-hidden="true"></span>
                                    </a>
                                </p>
                                <span class="glyphicon glyphicon-tags" aria-hidden="true"></span>
                                @if(!$book->tags->isEmpty())
                                    @foreach($book->tags as $tag)
                                        <span class="label label-primary tags">
                                                <a href="/api/books-tag/{{ $tag->tag }}">{{ $tag->tag }}</a>
                                            </span>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

        </div>

    </div>
@endsection
