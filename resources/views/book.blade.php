@extends('main')

@section('content')
    <div class="starter-template">
        <h1>{{ $book->name }}</h1>
        <div id="books" class="panel panel-default main-books">
            <div class="panel-heading">
                <h3 class="panel-title">{{ $book->title }}</h3>
            </div>
            <div class="panel-body">
                <div class="col-md-10">
                    <p>
                        <img src="{{ $book->image }}">
                    </p>
                    <strong>Автор:</strong>
                    <ul>
                        @foreach($book->authors as $author)
                            <li>{{ $author->name }}</li>
                        @endforeach
                    </ul>
                    <h3>Описание:</h3>
                    <p>{{ $book->description }}</p>
                    <p>

                    </p>
                    <span class="glyphicon glyphicon-tags" aria-hidden="true"></span>
                    @if(!$book->tags->isEmpty())
                        @foreach($book->tags as $tag)
                            <span class="label label-primary tags">
                                <a href="/api/books-tag/{{ $tag->tag }}">{{ $tag->tag }}</a>
                        </span>
                        @endforeach
                    @endif
                </div>
                <div class="col-md-2">
                    @if($book->status == \App\Models\Book::STATUS_ENABLE)
                        <p>Книга есть в наличии</p>
                        <button type="submit" id="crt" class="btn btn-success" data-book_id="{{ $book->id }}">
                            Положить в корзину
                        </button>
                    @else
                        <p>Книги нет в наличии</p>
                        @if(isset($book->subscribe))
                            <button type="submit" id="sub" class="btn btn-warning"
                                    data-book_id="{{ $book->id }}">
                                Отписаться
                            </button>
                        @else
                            <button type="submit" id="sub" class="btn btn-primary"
                                    data-book_id="{{ $book->id }}">
                                Подписаться
                            </button>
                        @endif
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
