<?php

use Illuminate\Database\Seeder;
use App\Models\Book;
use App\Models\Author;
use App\Models\Tag;
use Faker\Factory as Faker;

class TestDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        /**
         * Create authors
         */
        foreach (range(1, 20) as $index) {
            $author = Author::create([
                'name' => $faker->name
            ]);

            $author_id[] = $author->id;
        }

        /**
         * Create tags
         */
        foreach (range(1, 20) as $index) {
            $tag = Tag::create([
                'tag' => $faker->word
            ]);

            $tag_id[] = $tag->id;
        }

        /**
         * Create books
         */
        foreach (range(1, 21) as $index) {
            $book = Book::create([
                'status' => rand(Book::STATUS_DISABLE, Book::STATUS_ENABLE),
                'name' => $faker->sentence(),
                'title' => $faker->sentence(),
                'image' => '/upload/image.jpg',
                'slug' => $faker->slug(3),
                'description' => $faker->text(200)
            ]);

            $book->authors()->attach($faker->randomElements($author_id, rand(1, 3)));
            $book->tags()->attach($faker->randomElements($tag_id, rand(2, 6)));
        }
    }
}
