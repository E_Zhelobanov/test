<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Presenters\BookPresenter;
use Illuminate\Http\Request;

class BookController extends Controller
{
    /**
     * Get all books
     *
     * @param BookPresenter $presenter
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(BookPresenter $presenter)
    {
        $books = $presenter->getBooks();
        
        return response()->json($books);
    }
    
    /**
     * Get book by ID
     *
     * @param BookPresenter $presenter
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(BookPresenter $presenter, $id)
    {
        $book = $presenter->getBookById($id);
        
        return view('book', compact('book'));
    }
    
    /**
     * Show all books by tag
     *
     * @param BookPresenter $presenter
     * @param string $tag
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function tag(BookPresenter $presenter, $tag)
    {
        $tag = $presenter->getBooksByTag($tag);
        
        return view('tag', compact('tag'));
    }
    
    /**
     * Subscribe to book
     *
     * @param Request $request
     * @param BookPresenter $presenter
     * @param int $id
     */
    public function subscribe(BookPresenter $presenter, $id)
    {
        $presenter->subscribeBook($id);
    }
    
    /**
     * Add book to card
     *
     * @param Request $request
     * @param BookPresenter $presenter
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function addCart(BookPresenter $presenter, $id)
    {
        $cart = $presenter->bookAddToCart($id);
        
        return response()->json($cart);
        
    }
    
    /**
     * Get my cart
     *
     * @param BookPresenter $presenter
     * @return \Illuminate\Http\JsonResponse
     */
    public function showCart(BookPresenter $presenter)
    {
        $cart = $presenter->getMyCart();
        
        return response()->json($cart);
    }
    
    /**
     * Clear cart
     *
     * @param BookPresenter $presenter
     * @param Request $request
     */
    public function clearCart(BookPresenter $presenter)
    {
        $presenter->clearMyCart();
    }
}
